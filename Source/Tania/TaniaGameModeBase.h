// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TaniaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TANIA_API ATaniaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
